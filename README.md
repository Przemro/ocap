![OCAP](https://i.imgur.com/4Z16B8J.png)

**Operation Capture And Playback (BETA)**

![OCAP Screenshot](https://i.imgur.com/67L12wKl.jpg)

**[Live Web Demo](http://91.121.179.77:8000/)**

## What is it?
OCAP lets you **record and replay** operations on an interactive (web-based) map.
Reveal where the enemy were located.

Discover how each group carried out their assaults.

See who engaged who, when, and what with.

Use it simply for fun or as a training tool to see how well your group performs on ops.


## Overview

* Interactive web-based playback. All you need is a browser.
* Captures positions of all units and vehicles throughout an operation.
* Captures events such as shots fired, kills, and hits.
* Event log displays events as they happened in realtime.
* Clicking on a unit lets you follow them.
* Server based capture - no mods required for clients.

## Installation
### Requirements
* An Arma 3 game server
* A web server that you have SSH access to

### Arma server setup
1. Put `@ocap` and `@ocap/userconfig` folders into your server's Arma root directory
1. Put `ocap_exporter_x64.dll` into your server's Arma root directory
1. Configure the userconfig file
1. Launch server with `@ocap` enabled

Capture automatically begins when server becomes populated (see userconfig for settings).
Capture will stop on mission end/server close.

### Web server setup
1. Ensure you have Python 3.x, Node.js and [DockerTools](https://docs.docker.com/toolbox/overview/) installed
1. Run Windows PowerShell
1. Go to "webserver" loaction 'cd ...'
1. Configure `config.py` (can leave as default)
1. Run `docker-compose up`
1. Your webserver should be running and should be visible at localhost:8000

#### Arma Tile Creation

For information about this, pleas look for Arma Tile Creation Guide.txt (ocap/tiles-tut/Arma Tile Creation Guide.txt)

The server is now running and listening for capture data from the Arma server(s).
Start your Arma server (with @ocap enabled) and play an MP mission.

## Credits

* [3 Commando Brigade](http://www.3commandobrigade.com/) for testing and support (thanks lads!).
* [S.D.S](https://www.sds-polska.pl/) for testing and help with fixing mod
* [Leaflet](http://leafletjs.com/) - an awesome JS interactive map library
* Maca134 for his tutorial on [writing Arma extensions in C#](http://maca134.co.uk/tutorial/write-an-arma-extension-in-c-sharp-dot-net/)
