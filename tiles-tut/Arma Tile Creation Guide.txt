New and simpler guide for map creation. 

You can find the older version below.

--------------------------------

Tools installation :

1. Install Python 2.7.16 64-bit: https://www.python.org/downloads/release/python-2716/

2. Install newest Irfan View 64-bit with all extensions : https://www.fosshub.com/IrfanView.html

3. Download and install newest GDAL library ( 64-bit version ) and GDAL Python bindings for Python 2.7.* from https://www.gisinternals.com/release.php . 
   Here are the files you need to download ( there may be newer versions ) :
   - gdal-204-1911-x64-core.msi ( generic installer for the GDAL core components )
   - GDAL-2.4.0.win-amd64-py2.7.msi ( installer for the GDAL python bindings - requires you to install the GDAL core first )

--------------------------------

Map generation :

1.If you use maps from Steam Workshop - copy the mods to c:\Users\<user_name>\Documents\Arma 3
	Change the name of the mods, so that they contain no spaces ( e.g. change "@Wake Island" to "@Wake_Island" ).
	Create a shortcut, so that you are able to run Arma with mods ( e.g. "C:\Program Files (x86)\Steam\steamapps\common\Arma 3\arma3_x64.exe" -mod=@Wake_Island; ). Right click on shortcut - RUN IT AS ADMINISTRATOR. 

2. Open desired map in 2D editor (does not work in 3D editor).
	Note: 2D editor can be accessed by pressing Ctrl+O on map selection screen.

3. Press Left Shift + Numpad Minus (-).
	Release both keys and then blindly type EXPORTNOGRID
	An "Activated EXPORTNOGRID" message should (very briefly) appear.

4. Go to C:\ and check for <worldname>_nogrid.emf. If no file is found, make sure you followed step 1, 2 and 3.
    Close Arma 3 (important). Copy your new file to a directory of your choice.

5. Open EMF file in IrfanView. Our current task is to resize the image and save it as png file.
   We have to adjust the size of the image according to these tips :
   
   - output image size should be the power of 2. 
   - width and height should be equal.
   - pick your output size from values in tile size chart shown below. Keep the zoom level in memory.
   
   // Tile size chart
   Zoom Level    Output size ( in pixels )
   0             256
   1             512
   2             1024
   3             2048
   4             4096
   5             8192
   6             16384
   7             32768
   8             65536
   
   - output image size should be as close to input image size as possible, but prefer
     smaller sizes, because each next zoom level enlarges the output disk size four times.
	 In short - you will get more detailed maps with 4x bigger size for each zoom level.

  To resize the image use menu option : "Image->Resize/Resample..."
  Save it as PNG file in the same directory ( e.g. as altis.png ). Don't use uppercase letters in filename.
	 
  Example : the size of the Altis_nogrid.emf is 23040x23040. This value lies between 6th and 7th level. 
  The disk size of the 6th level is 80 MB approximately. The size of the 7th level will take 
  about 320 MB of the disk space, while adding nothing new to the quality. 
  If 80 MB is too much for you ( because for example your disk capacity is limited ), 
  then maybe you should resize the image to 5th level. In that case, the whole Altis will
  take only about 20 MB of disk space.
	
  You can use other tools to resize the image ( emftopng if you feel adventurous today, but 64-bit IrfanView works best for me )
   
6. 	Generate tiles using gdal2tiles_legacy_no-tms.py. 
  
  python gdal2tiles_legacy_no-tms.py -p raster -z 0-X6 altis.png
  
  Use option -z in form of 0-X where X is your zoom level.
  You may be forced to write the full path to the file gdal2tiles_legacy_no-tms.py.
  I just ommited it for clarity.
  
  Example:
  
  python gdal2tiles_legacy_no-tms.py -p raster -z 0-6 altis.png
  
7. Write down following values. They will be used for later configuration of the map on WWW server:
  
  "worldSize"  = original size of the emf file
  "imageSize"  = output size of the png file
  "multiplier" = imageSize / worldSize
  
  Example : we generated altis.png with the size 16384x16384 ( 6th level ).
  Our configuration values :
  "worldSize": 23040,
  "imageSize": 16384,
  "multiplier": 0.7111111111111111

--------------------------------

Older guide for map creation :

--------------------------------

Source for tutorial on generating .emf file: https://community.bistudio.com/wiki/ArmA:_Cheats#TOPOGRAPHY

This guide assumes you have the program OSGeo4W installed.
(https://trac.osgeo.org/osgeo4w/)

1. 	Run Arma 3 as Administrator (right click on arma3.exe > Run as administrator).

2. 	Open desired map in 2D editor (does not work in 3D editor).
	Note: 2D editor can be accessed by pressing Ctrl + O on map selection screen.

3. 	Press Left Shift + Numpad Minus (-).
	Release both keys and then blindly type EXPORTNOGRID.
	An "Activated EXPORTNOGRID" message should (very briefly) appear.

4.	Go to C:\ and check for <worldname>.emf. If no file is found, make sure you followed step 1.
 	Close Arma 3 (important).

5. 	Download BI Tools (https://community.bistudio.com/wiki/BI_Tools_2.5)
	and locate EmfToPng.exe  (in Visitor 3 folder).
	Copy EmfToPng.exe to a place you have write access (without need to run as administrator).

6. 	Copy <worldname>.emf (from step 4) to same folder as where we just copied EmfToPng.exe.
	Create a shortcut of EmfToPng.exe that launches with arguments: <worldname>.emf N
	
	N is the zoom level. We want to find the N which results in an image with dimensions that adheres to
	the tile size chart (see below). This may take some experimenting and will vary between maps.
	N is usually somewhere between 0.1 and 2. The recommended target is 16384.
	A good place to start is N=1. From there, calculate which zoom level is needed to adhere
	to tile size chart.
	e.g. If N=1 results in image size of 20480, and we want zoom level 6 (16384), then we'd do
	16384/20480 = 0.8 = N
	
	Note: The resulting image dimensions (both X and Y) must equal *exactly* 16384.
	If the dimensions of the image are not even, you will need to trim the image using an image editing
	program such as Photoshop or GIMP (free). Crop the image to remove any black borders/edges so that the
	resulting image has equal dimensions. You *must* ensure that any antialiasing features are disabled when
	doing this:
		- In Photoshop, use the select tool and NOT the crop tool.
		- In GIMP, use the crop tool.

7. 	Run shortcut and wait until .emf is converted to a .png

8. 	Drag and drop <worldname>.png into OSGeo4W64 directory.
	Launch OSGeo4W64.bat and enter the followig command:
	gdal2tiles_legacy_no-tms -p raster -z 0-6 <worldname>.png
	(assuming your image size is 16384 - zoom level 6)

	Note: gdal2tiles_legacy_no-tms is a modified version of gdal2tiles.
	To install, just drag the included gdal2tiles_legacy_no-tms .py and .bat
	files into OSGeo4W64\bin\.
	
If you wish to submit your tileset to be added to the OCAP map repository, please archive the folder
into a .tar file and upload somewhere (e.g. Dropbox). Ensure the name of the folder matches the
name of the terrian, and is in lowercase (e.g. isladuala3). Then please submit it via this GitHub ticket:
https://github.com/mistergoodson/OCAP/issues/34
You must include details on:
	- World name (e.g. Stratis)
	- Image size (size of image used to create tiles, e.g. 16384)
	- World size (size of image when N=1). Note: Please do not rely on the 'worldSize' value in Arma.
	- Multiplier (value of N)
Ideally, please provide info in the following example format:
{
	"name": "Isla Duala 3",
	"worldName": "isladuala3",
	"worldSize": 10240,
	"imageSize": 16384,
	"multiplier": 1.6
}


// Tile size chart
Zoom Level    Pixel size
0             256
1             512
2             1024
3             2048
4             4096
5             8192
6             16384
7             32768
8             65536


// Run the below code in the debug console to extract metadata on currently loaded maps.
_worlds = "true" configClasses (configFile >> "CfgWorldList");
worldArray = [];
{
	_x = configFile >> "CfgWorlds" >> (configName _x);
	worldArray pushBack [configName _x, getText (_x >> "description")];
} forEach _worlds;
copyToClipBoard str worldArray;

// Example output
["isladuala3","Isla Duala 3"] // Map config name, map display name
